<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>CanvasGridView</name>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3116"/>
        <location filename="../view/canvasgridview.cpp" line="3293"/>
        <source>Icon size</source>
        <translation>Icon size</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3122"/>
        <location filename="../view/canvasgridview.cpp" line="3301"/>
        <source>Auto merge</source>
        <translation>Auto merge</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3137"/>
        <location filename="../view/canvasgridview.cpp" line="3313"/>
        <source>Auto arrange</source>
        <translation>Auto arrange</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3194"/>
        <location filename="../view/canvasgridview.cpp" line="3198"/>
        <location filename="../view/canvasgridview.cpp" line="3366"/>
        <location filename="../view/canvasgridview.cpp" line="3371"/>
        <source>Set Wallpaper</source>
        <translation>Set Wallpaper</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3200"/>
        <location filename="../view/canvasgridview.cpp" line="3369"/>
        <source>Wallpaper and Screensaver</source>
        <translation>Wallpaper and Screensaver</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3513"/>
        <source>Properties</source>
        <translation>Properties</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3177"/>
        <location filename="../view/canvasgridview.cpp" line="3350"/>
        <source>Display Settings</source>
        <translation>Display Settings</translation>
    </message>
    <message>
        <location filename="../view/canvasgridview.cpp" line="3186"/>
        <source>Corner Settings</source>
        <translation>Corner Settings</translation>
    </message>
</context>
<context>
    <name>DesktopItemDelegate</name>
    <message>
        <location filename="../view/desktopitemdelegate.cpp" line="40"/>
        <source>Tiny</source>
        <translation>Tiny</translation>
    </message>
    <message>
        <location filename="../view/desktopitemdelegate.cpp" line="41"/>
        <source>Small</source>
        <translation>Small</translation>
    </message>
    <message>
        <location filename="../view/desktopitemdelegate.cpp" line="42"/>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <location filename="../view/desktopitemdelegate.cpp" line="43"/>
        <source>Large</source>
        <translation>Large</translation>
    </message>
    <message>
        <location filename="../view/desktopitemdelegate.cpp" line="44"/>
        <source>Super large</source>
        <translation>Super large</translation>
    </message>
</context>
<context>
    <name>DesktopMain</name>
    <message>
        <location filename="../main.cpp" line="149"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
</context>
<context>
    <name>Frame</name>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="683"/>
        <source>Wallpaper Slideshow</source>
        <translation>Wallpaper Slideshow</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="728"/>
        <source>When login</source>
        <translation>When login</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="730"/>
        <source>When wakeup</source>
        <translation>When wakeup</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="785"/>
        <source>Require a password on wakeup</source>
        <translation>Require a password on wakeup</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="817"/>
        <source>Never</source>
        <translation>Never</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="819"/>
        <source>Wait:</source>
        <translation>Wait:</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="852"/>
        <source>Wallpaper</source>
        <translation>Wallpaper</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="867"/>
        <source>Screensaver</source>
        <translation>Screensaver</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="943"/>
        <source>Only desktop</source>
        <translation>Only desktop</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="944"/>
        <source>Only lock screen</source>
        <translation>Only lock screen</translation>
    </message>
    <message>
        <location filename="../../dde-wallpaper-chooser/frame.cpp" line="996"/>
        <source>Apply</source>
        <translation>Apply</translation>
    </message>
</context>
<context>
    <name>WaterMaskFrame</name>
    <message>
        <location filename="../view/watermaskframe.cpp" line="253"/>
        <source>Not authorized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/watermaskframe.cpp" line="261"/>
        <source>In trial period</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ZoneMainWindow</name>
    <message>
        <location filename="../../dde-zone/mainwindow.cpp" line="64"/>
        <source>Fast Screen Off</source>
        <translation>Fast Screen Off</translation>
    </message>
    <message>
        <location filename="../../dde-zone/mainwindow.cpp" line="64"/>
        <source>All Windows</source>
        <translation>All Windows</translation>
    </message>
    <message>
        <location filename="../../dde-zone/mainwindow.cpp" line="64"/>
        <source>Launcher</source>
        <translation>Launcher</translation>
    </message>
    <message>
        <location filename="../../dde-zone/mainwindow.cpp" line="64"/>
        <source>Desktop</source>
        <translation>Desktop</translation>
    </message>
    <message>
        <location filename="../../dde-zone/mainwindow.cpp" line="64"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../../dde-zone/mainwindow.cpp" line="67"/>
        <source>Close Window</source>
        <translation>Close Window</translation>
    </message>
</context>
</TS>
